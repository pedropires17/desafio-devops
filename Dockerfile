FROM ubuntu
LABEL maintainer="Pedro"
RUN apt-get update && apt-get install -y nginx
RUN apt-get clean
COPY ./index.php /usr/share/nginx/html/
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]